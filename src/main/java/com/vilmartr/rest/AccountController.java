package com.vilmartr.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

public class AccountController {

    @GetMapping(value = "/processo/destinatario-padrao")
    public ResponseEntity<DestinatarioEnum> recuperarDestinatarioPadrao() {
        return RestResponses.ok();
    }

    @PostMapping(value = "/processo/label-investigado")
    public ResponseEntity<Map> recuperarLabelInvestigado(@RequestBody LabelInvestigado label) {
        return RestResponses.ok();
    }
}
